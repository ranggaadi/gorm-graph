package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"errors"
	"fmt"
	"goqlgen-trial/controllers"
	"goqlgen-trial/db"
	"goqlgen-trial/graph/generated"
	"goqlgen-trial/graph/model"
	"goqlgen-trial/models"
	"strconv"
	"strings"
	"time"

	"github.com/asaskevich/govalidator"
	libSlug "github.com/gosimple/slug"
)

func (r *mutationResolver) SignUp(ctx context.Context, input model.NewUser) (*model.User, error) {
	gc, err := controllers.RetrieveGinContext(ctx)
	if err != nil {
		return nil, err
	}

	bio := ""
	userImg := "avatar-default.jpg"
	role := "USER"

	if input.Role != nil {
		role = input.Role.String()
	}
	if input.Biography != nil {
		bio = *input.Biography
	}
	if input.UserImage != nil {
		userImg = *input.UserImage
	}

	user := models.User{
		Name:                input.Name,
		Username:            input.Username,
		Biography:           bio,
		UserImage:           userImg,
		Email:               input.Email,
		Role:                role,
		Password:            input.Password,
		ConfirmPassword:     input.ConfirmPassword,
		PasswordChangedAt:   nil,
		PasswordResetToken:  "",
		PasswordResetExpire: nil,
		Active:              true,
	}

	if err := user.Save(); err != nil {
		return nil, err
	}

	if err := controllers.SendToken(gc, user.ID); err != nil {
		return nil, err
	}

	return &model.User{
		ID:                  strconv.FormatUint(user.ID, 10),
		Name:                user.Name,
		Username:            user.Username,
		Biography:           user.Biography,
		UserImage:           user.UserImage,
		Email:               user.Email,
		Role:                model.Role(user.Role),
		Password:            user.Password,
		ConfirmPassword:     user.ConfirmPassword,
		PasswordChangedAt:   user.PasswordChangedAt,
		PasswordResetToken:  user.PasswordResetToken,
		PasswordResetExpire: user.PasswordResetExpire,
		Active:              user.Active,
		CreatedAt:           user.CreatedAt,
		UpdatedAt:           user.UpdatedAt,
	}, nil
	// panic(fmt.Errorf("not implemented"))
}

func (r *mutationResolver) CreateArticle(ctx context.Context, input model.NewArticle) (*model.Article, error) {
	gc, err := controllers.RetrieveGinContext(ctx)
	if err != nil {
		return nil, err
	}

	loggedUser, err := controllers.Protect(gc)
	if err != nil {
		return nil, err
	}

	cvr := "cover-default.jpg"
	usr := models.User{}
	tagStr := []*string{}

	if input.CoverImage != nil {
		cvr = *input.CoverImage
	}

	article := &models.Article{
		Title:      input.Title,
		Body:       input.Body,
		CoverImage: cvr,
		UserID:     loggedUser.ID,
	}

	if err := article.Save(); err != nil {
		return nil, err
	}

	if input.Tags != nil {
		if *input.Tags != "" {
			strTags := strings.Split(strings.ReplaceAll(*input.Tags, " ", ""), ",")
			for _, v := range strTags {
				tag, err := models.CreateTagIfNotExist(v)
				if err != nil {
					return nil, err
				}
				if err = article.AddTag(tag); err != nil {
					return nil, err
				}
			}
		}
	}

	db.Conn.Model(article).Related(&usr)

	if len(article.Tags) != 0 {
		for _, v := range article.Tags {
			tagStr = append(tagStr, &v.Tag)
		}
	}

	return &model.Article{
		ID:         strconv.FormatUint(article.ID, 10),
		Title:      article.Title,
		Slug:       article.Slug,
		Body:       article.Body,
		CoverImage: article.CoverImage,
		Author: &model.User{
			ID:                  strconv.FormatUint(usr.ID, 10),
			Name:                usr.Name,
			Username:            usr.Username,
			Biography:           usr.Biography,
			UserImage:           usr.UserImage,
			Email:               usr.Email,
			Role:                model.Role(usr.Role),
			Password:            usr.Password,
			ConfirmPassword:     usr.ConfirmPassword,
			PasswordChangedAt:   usr.PasswordChangedAt,
			PasswordResetToken:  usr.PasswordResetToken,
			PasswordResetExpire: usr.PasswordResetExpire,
			Active:              usr.Active,
			CreatedAt:           usr.CreatedAt,
			UpdatedAt:           usr.UpdatedAt,
		},
		Tags:      tagStr,
		CreatedAt: article.CreatedAt,
		UpdatedAt: article.UpdatedAt,
	}, nil
	// panic(fmt.Errorf("not implemented"))
}

func (r *mutationResolver) Login(ctx context.Context, input model.Login) (*model.User, error) {
	gc, err := controllers.RetrieveGinContext(ctx)
	if err != nil {
		return nil, err
	}

	user := models.User{}

	if govalidator.IsEmail(input.Credential) {
		if err := user.FindOneBy("email", input.Credential); err != nil {
			return nil, errors.New("Incorrect Incorrect email, username or password")
		}
	} else {
		if err := user.FindOneBy("username", input.Credential); err != nil {
			return nil, errors.New("Incorrect Incorrect email, username or password")
		}
	}

	if !user.Active || !user.CheckPassword(input.Password) {
		return nil, errors.New("Incorrect Incorrect email, username or password")
	}

	if err := controllers.SendToken(gc, user.ID); err != nil {
		return nil, err
	}

	return &model.User{
		ID:                  strconv.FormatUint(user.ID, 10),
		Name:                user.Name,
		Username:            user.Username,
		Biography:           user.Biography,
		UserImage:           user.UserImage,
		Email:               user.Email,
		Role:                model.Role(user.Role),
		Password:            user.Password,
		ConfirmPassword:     user.ConfirmPassword,
		PasswordChangedAt:   user.PasswordChangedAt,
		PasswordResetToken:  user.PasswordResetToken,
		PasswordResetExpire: user.PasswordResetExpire,
		Active:              user.Active,
		CreatedAt:           user.CreatedAt,
		UpdatedAt:           user.UpdatedAt,
	}, nil
	// panic(fmt.Errorf("not implemented"))
}

func (r *mutationResolver) UpdateArticle(ctx context.Context, slug string, input model.UpdateArticle) (*model.Article, error) {
	gc, err := controllers.RetrieveGinContext(ctx)
	if err != nil {
		return nil, err
	}

	loggedUser, err := controllers.Protect(gc)
	if err != nil {
		return nil, err
	}

	article := &models.Article{}
	usr := &models.User{}
	tagStr := []*string{}
	newSlug := ""

	if err := article.FindBySlug(slug); err != nil {
		return nil, err
	}

	db.Conn.Model(article).Related(usr)

	if loggedUser.ID != usr.ID && !loggedUser.IsAdmin() {
		return nil, errors.New("Unauthorized to update or delete this article")
	}

	// *** default If Empty ***

	if input.Title == nil || *input.Title == "" {
		input.Title = &article.Title
		newSlug = article.Slug
	} else if input.Title != nil {
		newSlug = fmt.Sprintf("%s-%d", libSlug.Make(*input.Title), time.Now().Unix())
	}

	if input.Body == nil || *input.Body == "" {
		input.Body = &article.Body
	}

	if input.CoverImage != nil && *input.CoverImage == "" {
		defaultImg := "cover-default.jpg"
		input.CoverImage = &defaultImg
	} else if input.CoverImage == nil {
		input.CoverImage = &article.CoverImage
	}

	// *** ============== ***

	if err := article.Update(map[string]interface{}{
		"title":       *input.Title,
		"body":        *input.Body,
		"cover_image": *input.CoverImage,
		"slug":        newSlug,
	}); err != nil {
		return nil, err
	}

	if input.Tags != nil {
		if *input.Tags != "" {
			strTags := strings.Split(strings.ReplaceAll(*input.Tags, " ", ""), ",")
			tags := []*models.Tag{}
			for _, v := range strTags {
				tag, err := models.CreateTagIfNotExist(v)
				if err != nil {
					return nil, err
				}
				tags = append(tags, tag)
			}
			if err = article.ReplaceTag(tags); err != nil {
				return nil, err
			}
		} else {
			if err = article.RemoveAllTag(); err != nil {
				return nil, err
			}
		}
	}

	for _, v := range article.Tags {
		tagStr = append(tagStr, &v.Tag)
	}

	return &model.Article{
		ID:         strconv.FormatUint(article.ID, 10),
		Title:      article.Title,
		Slug:       article.Slug,
		Body:       article.Body,
		CoverImage: article.CoverImage,
		Author: &model.User{
			ID:                  strconv.FormatUint(usr.ID, 10),
			Name:                usr.Name,
			Username:            usr.Username,
			Biography:           usr.Biography,
			UserImage:           usr.UserImage,
			Email:               usr.Email,
			Role:                model.Role(usr.Role),
			Password:            usr.Password,
			ConfirmPassword:     usr.ConfirmPassword,
			PasswordChangedAt:   usr.PasswordChangedAt,
			PasswordResetToken:  usr.PasswordResetToken,
			PasswordResetExpire: usr.PasswordResetExpire,
			Active:              usr.Active,
			CreatedAt:           usr.CreatedAt,
			UpdatedAt:           usr.UpdatedAt,
		},
		Tags:      tagStr,
		CreatedAt: article.CreatedAt,
		UpdatedAt: article.UpdatedAt,
	}, nil
}

func (r *mutationResolver) DeleteArticle(ctx context.Context, slug string) (*model.Article, error) {
	gc, err := controllers.RetrieveGinContext(ctx)
	if err != nil {
		return nil, err
	}

	loggedUser, err := controllers.Protect(gc)
	if err != nil {
		return nil, err
	}

	article := &models.Article{}
	usr := &models.User{}
	tags := []*string{}
	var returned *models.Article

	if err := article.FindBySlug(slug); err != nil {
		return nil, err
	}

	returned = article //copy
	db.Conn.Model(returned).Related(usr)
	for _, v := range returned.Tags {
		tags = append(tags, &v.Tag)
	}

	if loggedUser.ID != usr.ID && !loggedUser.IsAdmin() {
		return nil, errors.New("Unauthorized to update or delete this article")
	}

	if err := article.Delete(); err != nil {
		return nil, err
	}

	return &model.Article{
		ID:         strconv.FormatUint(returned.ID, 10),
		Title:      returned.Title,
		Slug:       returned.Slug,
		Body:       returned.Body,
		CoverImage: returned.CoverImage,
		Author: &model.User{
			ID:                  strconv.FormatUint(usr.ID, 10),
			Name:                usr.Name,
			Username:            usr.Username,
			Biography:           usr.Biography,
			UserImage:           usr.UserImage,
			Email:               usr.Email,
			Role:                model.Role(usr.Role),
			Password:            usr.Password,
			ConfirmPassword:     usr.ConfirmPassword,
			PasswordChangedAt:   usr.PasswordChangedAt,
			PasswordResetToken:  usr.PasswordResetToken,
			PasswordResetExpire: usr.PasswordResetExpire,
			Active:              usr.Active,
			CreatedAt:           usr.CreatedAt,
			UpdatedAt:           usr.UpdatedAt,
		},
		Tags:      tags,
		CreatedAt: returned.CreatedAt,
		UpdatedAt: returned.UpdatedAt,
	}, nil
	// panic(fmt.Errorf("not implemented"))
}

func (r *mutationResolver) Logout(ctx context.Context) (string, error) {
	gc, err := controllers.RetrieveGinContext(ctx)
	if err != nil {
		return "", err
	}

	gc.SetCookie("graph-jwt",
		"8ttp0nlyb0yz",
		-10,
		"/",
		strings.Split(gc.Request.Host, ":")[0], //localhost //semoga bener
		false,                                  //secure
		true,                                   //httponly
	)

	return "Successfully logged out", nil
	// panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) Users(ctx context.Context) ([]*model.User, error) {
	gc, err := controllers.RetrieveGinContext(ctx)
	if err != nil {
		return nil, err
	}

	loggedUser, err := controllers.Protect(gc)
	if err != nil {
		return nil, err
	}

	if !loggedUser.IsAdmin() {
		return nil, errors.New("You are unauthorized to access this query")
	}

	var result []*model.User
	var queryRes []*models.User
	db.Conn.Find(&queryRes)

	for _, usr := range queryRes {
		result = append(result, &model.User{
			ID:                  strconv.FormatUint(usr.ID, 10),
			Name:                usr.Name,
			Username:            usr.Username,
			Biography:           usr.Biography,
			UserImage:           usr.UserImage,
			Email:               usr.Email,
			Role:                model.Role(usr.Role),
			Password:            usr.Password,
			ConfirmPassword:     usr.ConfirmPassword,
			PasswordChangedAt:   usr.PasswordChangedAt,
			PasswordResetToken:  usr.PasswordResetToken,
			PasswordResetExpire: usr.PasswordResetExpire,
			Active:              usr.Active,
			CreatedAt:           usr.CreatedAt,
			UpdatedAt:           usr.UpdatedAt,
		})
	}

	return result, nil
	// panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) User(ctx context.Context, id *int, username *string) (*model.User, error) {
	gc, err := controllers.RetrieveGinContext(ctx)
	if err != nil {
		return nil, err
	}

	_, err = controllers.Protect(gc)
	if err != nil {
		return nil, err
	}

	user := &models.User{}

	if id == nil && username == nil {
		return nil, errors.New("Please fill parameter with id or username")
	}

	if id != nil && username != nil {
		return nil, errors.New("Please fill parameter with id or username")
	} else if id != nil {
		if err := user.FindById(*id); err != nil {
			return nil, err
		}
	} else {
		if err := user.FindOneBy("username", *username); err != nil {
			return nil, err
		}
	}

	return &model.User{
		ID:                  strconv.FormatUint(user.ID, 10),
		Name:                user.Name,
		Username:            user.Username,
		Biography:           user.Biography,
		UserImage:           user.UserImage,
		Email:               user.Email,
		Role:                model.Role(user.Role),
		Password:            user.Password,
		ConfirmPassword:     user.ConfirmPassword,
		PasswordChangedAt:   user.PasswordChangedAt,
		PasswordResetToken:  user.PasswordResetToken,
		PasswordResetExpire: user.PasswordResetExpire,
		Active:              user.Active,
		CreatedAt:           user.CreatedAt,
		UpdatedAt:           user.UpdatedAt,
	}, nil
	// panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) Articles(ctx context.Context, tag *string, title *string) ([]*model.Article, error) {
	var articles []*model.Article
	var query []*models.Article
	var seacrhedTags models.Tag

	if tag == nil && title == nil {
		db.Conn.Preload("Tags").Find(&query)
	} else if tag != nil && title != nil {
		db.Conn.Where("tag = ?", *tag).First(&seacrhedTags)
		db.Conn.Model(&seacrhedTags).Preload("Tags").Where("title LIKE ?", fmt.Sprintf("%%%s%%", *title)).Related(&query, "Articles")
	} else if title != nil {
		db.Conn.Preload("Tags").Where("title LIKE ?", fmt.Sprintf("%%%s%%", *title)).Find(&query)
	} else if tag != nil {
		db.Conn.Where("tag = ?", *tag).First(&seacrhedTags)
		db.Conn.Model(&seacrhedTags).Preload("Tags").Related(&query, "Articles")
	}

	for _, art := range query {
		tags := []*string{}
		usr := models.User{}

		if len(art.Tags) != 0 {
			for _, v := range art.Tags {
				tags = append(tags, &v.Tag)
			}
		}

		db.Conn.Model(art).Related(&usr)

		articles = append(articles, &model.Article{
			ID:         strconv.FormatUint(art.ID, 10),
			Title:      art.Title,
			Body:       art.Body,
			Slug:       art.Slug,
			CoverImage: art.CoverImage,
			Author: &model.User{
				ID:                  strconv.FormatUint(usr.ID, 10),
				Name:                usr.Name,
				Username:            usr.Username,
				Biography:           usr.Biography,
				UserImage:           usr.UserImage,
				Email:               usr.Email,
				Role:                model.Role(usr.Role),
				Password:            usr.Password,
				ConfirmPassword:     usr.ConfirmPassword,
				PasswordChangedAt:   usr.PasswordChangedAt,
				PasswordResetToken:  usr.PasswordResetToken,
				PasswordResetExpire: usr.PasswordResetExpire,
				Active:              usr.Active,
				CreatedAt:           usr.CreatedAt,
				UpdatedAt:           usr.UpdatedAt,
			},
			Tags:      tags,
			CreatedAt: art.CreatedAt,
			UpdatedAt: art.UpdatedAt,
		})
	}

	return articles, nil
	// panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) Article(ctx context.Context, slug string) (*model.Article, error) {
	article := &models.Article{}
	usr := &models.User{}

	if err := article.FindBySlug(slug); err != nil {
		return nil, err
	}

	tags := []*string{}

	if len(article.Tags) != 0 {
		for _, v := range article.Tags {
			tags = append(tags, &v.Tag)
		}
	}

	db.Conn.Model(article).Related(usr)

	return &model.Article{
		ID:         strconv.FormatUint(article.ID, 10),
		Title:      article.Title,
		Body:       article.Body,
		Slug:       article.Slug,
		CoverImage: article.CoverImage,
		Author: &model.User{
			ID:                  strconv.FormatUint(usr.ID, 10),
			Name:                usr.Name,
			Username:            usr.Username,
			Biography:           usr.Biography,
			UserImage:           usr.UserImage,
			Email:               usr.Email,
			Role:                model.Role(usr.Role),
			Password:            usr.Password,
			ConfirmPassword:     usr.ConfirmPassword,
			PasswordChangedAt:   usr.PasswordChangedAt,
			PasswordResetToken:  usr.PasswordResetToken,
			PasswordResetExpire: usr.PasswordResetExpire,
			Active:              usr.Active,
			CreatedAt:           usr.CreatedAt,
			UpdatedAt:           usr.UpdatedAt,
		},
		Tags:      tags,
		CreatedAt: article.CreatedAt,
		UpdatedAt: article.UpdatedAt,
	}, nil
	// panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) Myarticle(ctx context.Context) ([]*model.Article, error) {
	gc, err := controllers.RetrieveGinContext(ctx)
	if err != nil {
		return nil, err
	}

	loggedUser, err := controllers.Protect(gc)
	if err != nil {
		return nil, err
	}

	currentUserArticles := []*models.Article{}
	userArticles := []*model.Article{}
	db.Conn.Model(loggedUser).Preload("Tags").Related(&currentUserArticles)

	for _, article := range currentUserArticles {
		tags := []*string{}

		if len(article.Tags) != 0 {
			for _, v := range article.Tags {
				tags = append(tags, &v.Tag)
			}
		}

		userArticles = append(userArticles, &model.Article{
			ID:         strconv.FormatUint(article.ID, 10),
			Title:      article.Title,
			Body:       article.Body,
			Slug:       article.Slug,
			CoverImage: article.CoverImage,
			Author: &model.User{
				ID:                  strconv.FormatUint(loggedUser.ID, 10),
				Name:                loggedUser.Name,
				Username:            loggedUser.Username,
				Biography:           loggedUser.Biography,
				UserImage:           loggedUser.UserImage,
				Email:               loggedUser.Email,
				Role:                model.Role(loggedUser.Role),
				Password:            loggedUser.Password,
				ConfirmPassword:     loggedUser.ConfirmPassword,
				PasswordChangedAt:   loggedUser.PasswordChangedAt,
				PasswordResetToken:  loggedUser.PasswordResetToken,
				PasswordResetExpire: loggedUser.PasswordResetExpire,
				Active:              loggedUser.Active,
				CreatedAt:           loggedUser.CreatedAt,
				UpdatedAt:           loggedUser.UpdatedAt,
			},
			Tags:      tags,
			CreatedAt: article.CreatedAt,
			UpdatedAt: article.UpdatedAt,
		})
	}

	return userArticles, nil
	// panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) Profile(ctx context.Context) (*model.User, error) {
	gc, err := controllers.RetrieveGinContext(ctx)
	if err != nil {
		return nil, err
	}

	loggedUser, err := controllers.Protect(gc)
	if err != nil {
		return nil, err
	}

	return &model.User{
		ID:                  strconv.FormatUint(loggedUser.ID, 10),
		Name:                loggedUser.Name,
		Username:            loggedUser.Username,
		Biography:           loggedUser.Biography,
		UserImage:           loggedUser.UserImage,
		Email:               loggedUser.Email,
		Role:                model.Role(loggedUser.Role),
		Password:            loggedUser.Password,
		ConfirmPassword:     loggedUser.ConfirmPassword,
		PasswordChangedAt:   loggedUser.PasswordChangedAt,
		PasswordResetToken:  loggedUser.PasswordResetToken,
		PasswordResetExpire: loggedUser.PasswordResetExpire,
		Active:              loggedUser.Active,
		CreatedAt:           loggedUser.CreatedAt,
		UpdatedAt:           loggedUser.UpdatedAt,
	}, nil
	// panic(fmt.Errorf("not implemented"))
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
