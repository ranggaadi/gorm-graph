package controllers

import (
	"errors"
	"fmt"
	"goqlgen-trial/db"
	"goqlgen-trial/models"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"

	"github.com/gin-gonic/gin"
)

func signToken(id uint64) (string, error) {
	expDuration, _ := strconv.Atoi(os.Getenv("JWT_EXPIRE_DURATION"))

	jwtSignatureKey := []byte(os.Getenv("JWT_SIGNATURE_KEY"))
	jwtExpireTime := time.Duration(expDuration) * time.Hour

	var claim models.Claims
	claim.ExpiresAt = time.Now().Add(jwtExpireTime).Unix()
	claim.UserId = id
	claim.IssuedAt = time.Now().Unix()

	signedToken, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claim).SignedString(jwtSignatureKey)
	if err != nil {
		return "", err
	}

	return signedToken, nil
}

func parseToken(bearerToken string) (jwt.MapClaims, error) {
	token, err := jwt.Parse(bearerToken, func(token *jwt.Token) (interface{}, error) {
		if jwt.GetSigningMethod("HS256") != token.Method {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(os.Getenv("JWT_SIGNATURE_KEY")), nil
	})

	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return nil, errors.New("Failed to claims token")
	}

	return claims, nil
}

func SendToken(c *gin.Context, id uint64) error {
	ckDuration, _ := strconv.Atoi(os.Getenv("JWT_COOKIE_DURATION"))

	token, err := signToken(id)
	if err != nil || token == "" {
		return err
	}

	c.SetCookie("graph-jwt",
		token,
		ckDuration*3600,
		"/",
		strings.Split(c.Request.Host, ":")[0], //localhost //semoga bener
		false,                                 //secure
		true,                                  //httponly
	)

	return nil
}

func Protect(c *gin.Context) (*models.User, error) {
	currentUser := models.User{}
	jwtToken, err := c.Cookie("graph-jwt")
	if err != nil {
		return nil, errors.New("You are not logged in please login first")
	}

	claims, err := parseToken(jwtToken)
	if err != nil {
		return nil, err
	}

	if err := db.Conn.First(&currentUser, uint64(claims["UserId"].(float64))); err.Error != nil {
		return nil, errors.New("user belonging to this token is no longer exist")
	}

	return &currentUser, nil
}
