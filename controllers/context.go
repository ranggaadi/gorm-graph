package controllers

import (
	"context"
	"fmt"

	"github.com/gin-gonic/gin"
)

func GinContextToMainContext() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := context.WithValue(c.Request.Context(), "ginContext", c)
		c.Request = c.Request.WithContext(ctx)
		c.Next()
	}
}

func RetrieveGinContext(ctx context.Context) (*gin.Context, error) {
	ginContext := ctx.Value("ginContext")
	if ginContext == nil {
		err := fmt.Errorf("could not retrieve gin.Context")
		return nil, err
	}

	gc, ok := ginContext.(*gin.Context)
	if !ok {
		err := fmt.Errorf("gin.Context has wrong type")
		return nil, err
	}
	return gc, nil
}
