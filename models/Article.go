package models

import (
	"fmt"
	"goqlgen-trial/db"
	"time"

	"github.com/gosimple/slug"
)

type Article struct {
	ID         uint64    `gorm:"primary_key" json:"id"`
	Title      string    `gorm:"not null" json:"title"`
	Slug       string    `gorm:"not null; unique_index" json:"slug"`
	Body       string    `gorm:"not null" json:"body"`
	CoverImage string    `gorm:"column:cover_image; not null; default:'cover-default.jpg'" json:"cover-image"`
	Author     User      `gorm:"foreignkey:UserID; not null" json:"author"`
	UserID     uint64    `gorm:"not null" json:"user_id"`
	Tags       []*Tag    `gorm:"many2many:article_tags;" json:"tags"`
	CreatedAt  time.Time `gorm:"default:current_timestamp()" json:"created_at"`
	UpdatedAt  time.Time `gorm:"default:current_timestamp()" json:"updated_at"`
}

func (a *Article) Save() error {
	a.Slug = fmt.Sprintf("%s-%d", slug.Make(a.Title), time.Now().Unix())

	if err := db.Conn.Create(a); err.Error != nil {
		return err.Error
	}

	return nil
}

func (a *Article) AddTag(tag *Tag) error {
	res := db.Conn.Model(a).Association("Tags").Append(tag)
	return res.Error
}

func (a *Article) ReplaceTag(tags []*Tag) error {
	res := db.Conn.Model(a).Association("Tags").Replace(tags)
	return res.Error
}

func (a *Article) RemoveAllTag() error {
	if assoc := db.Conn.Model(a).Association("Tags").Clear(); assoc.Error != nil {
		return assoc.Error
	}
	return nil
}

func (a *Article) FindBySlug(slug string) error {
	if res := db.Conn.Preload("Tags").Where("slug = ?", slug).First(a); res.Error != nil {
		return res.Error
	}

	return nil
}

func (a *Article) Update(art map[string]interface{}) error {
	if err := db.Conn.Model(a).Updates(art); err.Error != nil {
		return err.Error
	}

	return nil
}

func (a *Article) Delete() error {
	if err := a.RemoveAllTag(); err != nil {
		return err
	}

	if res := db.Conn.Delete(a); res.Error != nil {
		return res.Error
	}

	return nil
}
