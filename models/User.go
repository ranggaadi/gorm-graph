package models

import (
	"errors"
	"fmt"
	"goqlgen-trial/db"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID                  uint64     `gorm:"primary_key" json:"id"`
	Name                string     `gorm:"not null; size:100" json:"name"`
	Username            string     `gorm:"not null; unique" json:"username"`
	Biography           string     `gorm:"type:mediumtext; null" json:"biography"`
	UserImage           string     `gorm:"column:user_image; not null; default:'avatar-default.jpg'" json:"user-image"`
	Email               string     `gorm:"not null; unique_index" json:"email"`
	Role                string     `gorm:"not null; default:'USER'" json:"role"`
	Articles            []Article  `gorm:"foreignkey:UserID" json:"articles"`
	Password            string     `gorm:"not null" json:"password"`
	ConfirmPassword     string     `gorm:"column:confirm_password;" json:"confirm-password"`
	PasswordChangedAt   *time.Time `json:"passwordChangedAt"`
	PasswordResetToken  string     `json:"passwordResetToken"`
	PasswordResetExpire *time.Time `json:"passwordResetExpire"`
	Active              bool       `gorm:"not null; default:true" json:"active"`
	CreatedAt           time.Time  `gorm:"default:current_timestamp()" json:"created_at"`
	UpdatedAt           time.Time  `gorm:"default:current_timestamp()" json:"updated_at"`
}

func (u *User) Save() error {
	if !u.confirmPassword() {
		return errors.New("Confirm Password field is not match")
	}

	if err := u.hashPassword(); err != nil {
		return fmt.Errorf("Error hashing password: %v", err)
	}

	if err := db.Conn.Create(u); err.Error != nil {
		return err.Error
	}

	return nil
}

func (u *User) confirmPassword() bool {
	return u.Password == u.ConfirmPassword
}

func (u *User) hashPassword() error {
	bytes, err := bcrypt.GenerateFromPassword([]byte(u.Password), 12)
	if err != nil {
		return err
	}
	u.Password = string(bytes)
	u.ConfirmPassword = ""
	return nil
}

func (u *User) CheckPassword(candidate string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(candidate))
	return err == nil
}

func (u *User) FindById(id int) error {
	if res := db.Conn.First(u, id); res.Error != nil {
		return res.Error
	}

	return nil
}

func (u *User) FindOneBy(by, param string) error {
	if res := db.Conn.Where(fmt.Sprintf("%s = ?", by), param).First(u); res.Error != nil {
		return res.Error
	}

	return nil
}

func (u *User) IsAdmin() bool {
	return u.Role == "ADMIN"
}
