package models

import (
	"goqlgen-trial/db"
	"time"
)

type Tag struct {
	ID        uint64     `gorm:"primary_key" json:"id"`
	Tag       string     `gorm:"not null;unique" json:"tag"`
	Articles  []*Article `gorm:"many2many:article_tags;" json:"articles"`
	CreatedAt time.Time  `gorm:"default:current_timestamp()" json:"created_at"`
	UpdatedAt time.Time  `gorm:"default:current_timestamp()" json:"updated_at"`
}

func CreateTagIfNotExist(tagName string) (*Tag, error) {
	var tag Tag
	res := db.Conn.FirstOrCreate(&tag, Tag{Tag: tagName})
	if res.Error != nil {
		return nil, res.Error
	}
	return &tag, nil
}
