package main

import (
	"goqlgen-trial/controllers"
	"goqlgen-trial/db"
	"goqlgen-trial/graph"
	"goqlgen-trial/graph/generated"
	"goqlgen-trial/models"
	"log"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func graphqlHandler() gin.HandlerFunc {
	hndlr := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{}}))

	return func(c *gin.Context) {
		hndlr.ServeHTTP(c.Writer, c.Request)
	}
}

func playgroundHandler() gin.HandlerFunc {
	hndlr := playground.Handler("GraphQL playground", "/query")

	return func(c *gin.Context) {
		hndlr.ServeHTTP(c.Writer, c.Request)
	}
}

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	db.InitAndMigrateDB()
	if db.Conn != nil {
		db.Conn.AutoMigrate(&models.User{}, &models.Article{}, &models.Tag{})
	}

	r := gin.Default()
	r.Use(controllers.GinContextToMainContext())
	r.POST("/query", graphqlHandler())
	r.GET("/", playgroundHandler())
	r.Run()
}
